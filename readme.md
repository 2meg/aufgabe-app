# Devops notes:

### Implemented features:
1. Terraform project to provide the needed infrastructure 
2. Hosted Jenkins server and Nexus repository with a DNS name and SSL protection 
3. Jenkins pipeline for an automatic build and push to Dockerhub/Nexus on merge/push to the develop branch, both frontend and backend in two different docker images. The pipeline is designed for manual QA engineers to test the build until it gets to production. QA team gets notified with an automatic Jenkins notification in the QA slack channel. Then the QA engineer confirms that build works with the button on the pipeline, and then the pipeline proceeds to "deploy to production" build. The functionality to mark tests as failed and revert changes is yet to be implemented.
4. Dockerfiles with multi-stage building. Docker-compose file, intended to be used locally so it has MongoDB running on a container instead of the hosted MongoDB cluster
5. Starter-level ansible configuration files in a separate repository, to prepare environments and deploy the application on both qa and production environments
6. A separate Jenkins pipeline for ansible deployment
7. Each build gets tagged by incrementing version, specified at "package.json". Each build the version gets incremented and the version is pushed to the git repository.

### Repositories:

- https://gitlab.com/2meg/Aufgabe_Ansible - ansible config files
- https://gitlab.com/2meg/aufgabe-shared-library - a shared library for Jenkins common functions. Some functions are used in the Compito project as well
- https://gitlab.com/2meg/aufgabe-terraform - terraform deployment files

### Continuous deployment stages: 
0. A developer clones the main repository, and deploys local environment on his own laptop using docker-compose.
1. Terraform infrastructure gets initiated manually on my host PC
2. After a commit or an accepted merge request to the develop branch by a developer, a build pipeline is triggered. 
3. Both frontend and backend images get built and pushed to the dockerhub/nexus repository with the "develop" tag (in the video I use dockerhub)
4. The deploy stage on each environment uses a separate Jenkins pipeline: "ansible_aufgabe_stage" for qa environment "ansible_aufgabe_prod" for production. Each of them pulls the ansible configuration files, sends them on the Ansible server, and then executes deploy commands on that server that deploys the application on the testing and production environments
5. The testing environment is accessible at qa.aufgabe.myroslav-symchych.space and the production environment is accessible at www.aufgabe.myroslav-symchych.space ("A" records were created with terraform on route53 during the initial environment creation)
6. After the "deploy on testing environment" stage, the main pipeline gets frozen until QA validates new builds on the testing environment. QA team gets notified with an automatic Jenkins notification in the QA slack channel. Then QA team marks the test build as 'successful' at the main pipeline by an input pop-up
6. The tested build gets deployed on the production

Deployment process showcase: 
https://www.youtube.com/watch?v=ApG1RpboXBg&ab_channel=MyroslavS.
Nexus repository showcase: 
https://www.youtube.com/watch?v=xODjwDcOYf8&ab_channel=MyroslavS.
